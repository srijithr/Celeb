from collections import OrderedDict
from bokeh.sampledata import us_states, us_counties, unemployment
from bokeh.plotting import figure, show, output_file, ColumnDataSource
from bokeh.models import HoverTool
from bokeh.plotting import *
from bokeh.palettes import *
from bokeh.models import *
import pandas as p
import numpy as np
import colorsys
import json
import xlrd
import csv
	    		

def get_image_links():

	ent = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Dec02/Celebrity_Database__dec_02_ent.csv')
	ent.columns
	clean = ent[['Name', 'Category','Company','Food and beverage category','Product line','Ethnicity','Age','Unnamed: 7']].dropna(how='any')
#clean = ent[['Name','Company','Food and beverage category','Product line','Unnamed: 7','Unnamed: 8']].dropna(how='any')
#clean.rename(columns={'Unnamed: 7':'Ethnicity'},inplace=True)
	clean.rename(columns={'Unnamed: 7':'link'},inplace=True)
	clean.rename(columns={'Unnamed: 8':'Age'},inplace=True)
	clean['tag'] = 'ent'

	ath = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Dec02/Celebrity_Database__dec_02_ath.csv')
	clean_ath = ath[['Name', 'Category', 'Company', 'Food and beverage category','Product line','Code','Age','Unnamed: 7']].dropna(how='any')
	clean_ath.rename(columns={'Code':'Ethnicity'},inplace=True)
	clean_ath.rename(columns={'Unnamed: 7':'link'},inplace=True)
	clean_ath['tag'] = 'ath'
	
	oth = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Dec02/Celebrity_Database__dec_02_oth.csv')
	clean_oth = oth[['Name', 'Category', 'Company','Food and beverage category','Product line','Code','Age','Unnamed: 7']].dropna(how='any')
	clean_oth.rename(columns={'Code':'Ethnicity'},inplace=True)
	clean_oth.rename(columns={'Unnamed: 7':'link'},inplace=True)
	clean_oth['tag'] = 'oth'

	fnv = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Dec02/Celebrity_Database__dec_02_fnv.csv')
	clean_fnv = fnv[['Name', 'Category', 'Company','Food and beverage category','Product line','Code','Age','Unnamed: 7']].dropna(how='any')
	clean_fnv.rename(columns={'Code':'Ethnicity'},inplace=True)
	clean_fnv.rename(columns={'Unnamed: 7':'link'},inplace=True)
	clean_fnv['tag'] = 'fnv'
	
	milk = p.read_csv('/Users/srijithrajamohan/Documents/kraak/Dec02/Celebrity_Database__dec_02_milk.csv')
	clean_milk = milk[['#NAME?', 'Category', 'Company','Food and beverage category','Product line','Code ','Age','Unnamed: 7']].dropna(how='any')
	clean_milk.rename(columns={'Code ':'Ethnicity'},inplace=True)
	clean_milk['tag'] = 'milk'
	clean_milk.rename(columns={'#NAME?':'Name'},inplace=True)
	clean_milk.rename(columns={'Unnamed: 7':'link'},inplace=True)
	clean_ath.rename(columns={'Code':'Ethnicity'},inplace=True)

	clean['link'] = clean['link'].convert_objects(convert_numeric=True).dropna(how='any')
	clean.dropna(how='any',inplace=True)
	clean['link'].astype('int')

	clean_fnv['link'] = clean_fnv['link'].convert_objects(convert_numeric=True).dropna(how='any')
	clean_fnv.dropna(how='any',inplace=True)
	clean_fnv['link'].astype(int)

	clean_oth['link'] = clean_oth['link'].convert_objects(convert_numeric=True).dropna(how='any')
	clean_oth.dropna(how='any',inplace=True)
	clean_oth['link'].astype(int)

	clean_ath['link'] = clean_ath['link'].convert_objects(convert_numeric=True).dropna(how='any')
	clean_ath.dropna(how='any',inplace=True)
	clean_ath['link'].astype(int)

	clean_milk['link'] = clean_milk['link'].convert_objects(convert_numeric=True).dropna(how='any')
	clean_milk.dropna(how='any',inplace=True)
	clean_milk['link'].astype('int')

	
	flat = p.concat([clean,clean_ath,clean_oth,clean_fnv,clean_milk])
	flat.dropna(how='any')
	flat['Name'].describe() # get all unique names
	
	intv = np.vectorize(int)
	tlen = len(flat)
	flat.index = intv(np.linspace(1,tlen,tlen))
	
	flat['class'] = '0'
	flat[flat['Food and beverage category'].isin(['QSR','Confection or Savory Snack or Cereal'])]
	flat.loc[flat['Food and beverage category'].isin(['Low-cal & no-cal beverages','Low cal and no cal beverages','Low-cal & no-cal beverage','Dairy','Dairy ','water','Fruits and Vegetables','Fruit and Vegetables','Milk']),'class'] = 'p'
	flat.loc[flat['Food and beverage category'].isin(['SSB','QSR','Confection or Savory snack or Cereal','Confection or Savory Snack or Cereal','Cereal']),'class'] = 'n'
	flat_sub = flat[['Name','tag','class','link']]
	#flat_sub[np.isnan(flat_sub['link'])]
	for i in range(1,len(flat)):
		print i , flat_sub.loc[i]['link'] , flat_sub.loc[i]['tag']
	flat_sub['link'] = flat_sub['link'].astype(int)
	
	flat_sub.to_csv('clean_links.csv')
	with open("text", "w") as outfile:
		json.dump(flat_sub.to_json(),outfile)

	globals()['myvar'] = flat_sub


def get_json(myvar):
		df = p.DataFrame({
		"time" : myvar['Name'].values,
    	"temp" : myvar['link'].values
		})

		d = [ 
    		dict([
        	(colname, row[i]) 
        	for i,colname in enumerate(df.columns)
    		])
    	for row in df.values
		]
		return json.dumps(d)

def get_dict(myvar):
		df = p.DataFrame({
		"Name" : myvar['Name'].values,
    	"link" : myvar['link'].values
		})

		d = [ 
    		dict([
        	(colname, row[i]) 
        	for i,colname in enumerate(df.columns)
    		])
    	for row in df.values
		]
		return d				 



def get_csv_from_xls(xls_file):
   wb = xlrd.open_workbook(xls_file)

   j = 0
   for i in ['Entertainment','Athlete','Other','FNV','GotMilk']:
    sh = wb.sheet_by_index(j)
    j = j + 1
    your_csv_file = open(i + '.csv', 'wb')
    wr = csv.writer(your_csv_file, quoting=csv.QUOTE_ALL)
    for rownum in xrange(sh.nrows):
        print sh.row_values(rownum)
        wr.writerow(sh.row_values(rownum))

    your_csv_file.close()

get_csv_from_xls('/Users/srijithrajamohan/Documents/kraak/CelebrityDatabase.xlsx')
ent = p.read_csv('/Users/srijithrajamohan/Entertainment.csv')
ent.columns
clean = ent[['Name', 'Category','Company','Food and beverage category','Product line/Brand','Ethnicity','Age']].dropna(how='any')
#clean = ent[['Name','Company','Food and beverage category','Product line','Unnamed: 7','Unnamed: 8']].dropna(how='any')
#clean.rename(columns={'Unnamed: 7':'Ethnicity'},inplace=True)
clean.rename(columns={'Unnamed: 8':'Age'},inplace=True)
clean['tag'] = 'ent'


ath = p.read_csv('/Users/srijithrajamohan/Athlete.csv')
ath.columns
clean_ath = ath[['Name', 'Category', 'Company', 'Food and beverage category','Product line/Brand','Ethnicity','Age']].dropna(how='any')
clean_ath['tag'] = 'ath'

oth = p.read_csv('/Users/srijithrajamohan/Other.csv')
clean_oth = oth[['Name', 'Category', 'Company','Food and beverage category','Product line/Brand','Ethnicity','Age']].dropna(how='any')
clean_oth.rename(columns={'Code':'Ethnicity'},inplace=True)
clean_oth['tag'] = 'oth'

fnv = p.read_csv('/Users/srijithrajamohan/FNV.csv')
clean_fnv = fnv[['Name', 'Category', 'Company','Food and beverage category','Product line/Brand','Ethnicity','Age']].dropna(how='any')
clean_fnv['tag'] = 'fnv'

milk = p.read_csv('/Users/srijithrajamohan/GotMilk.csv')
clean_milk = milk[['Name', 'Category', 'Company','Food and beverage category','Product line/Brand','Ethnicity','Age']].dropna(how='any')
clean_milk['tag'] = 'milk'


flat = p.concat([clean,clean_ath,clean_oth,clean_fnv,clean_milk])
flat['Name'].describe() # get all unique names
flat.to_csv('clean.csv')

intv = np.vectorize(int)
tlen = len(flat)
flat.index = intv(np.linspace(1,tlen,tlen))
flat.to_csv('clean_unique.csv')

#flat.ix[12].Age = '34'
#flat.ix[189].Age =  '40'
flat.ix[219].Age = '40'
flat.Age = flat.Age.convert_objects(convert_numeric=True)

flat['class'] = '0'
flat[flat['Food and beverage category'].isin(['QSR','Confection or Savory Snack or Cereal'])]
flat.loc[flat['Food and beverage category'].isin(['Low-cal & no-cal beverages','Low cal and no cal beverages','Low-cal & no-cal beverage','Dairy','Dairy ','water','Fruits and Vegetables','Fruit and Vegetables','Milk']),'class'] = 'p'
flat.loc[flat['Food and beverage category'].isin(['SSB','QSR','Confection or Savory snack or Cereal','Confection or Savory Snack or Cereal','Cereal']),'class'] = 'n'
flat.to_csv('clean_unique.csv')

flat[flat['Name'].duplicated() == True]

mdat = p.DataFrame([])
multiple_endorsements = flat[flat['Name'].duplicated(take_last=True)]['Name'].drop_duplicates()
m = multiple_endorsements
for i in range(0,m.count()):
 temp = flat[p.Index(flat['Name']).get_loc(m[m.index[i]]) == True]
 print temp
 mdat = mdat.append(temp)

print mdat[['Name','class']]

for i in flat.columns:
  print i
  print flat[p.isnull(flat[i]) == True]

get_image_links() 


positive = myvar[myvar['class'] == 'p']
pjson = get_json(positive)
negative = myvar[myvar['class'] == 'n']
njson = get_json(negative)

a = get_dict(positive)
b = get_dict(negative)
final = { "Positive" : a , "Negative" : b }
with open("my.json","w") as f:
    json.dump(final,f)


unique_no_celebs = len(flat.Name.unique())
unique_no_ent = len(ent.Name.unique())
unique_no_ath = len(ath.Name.unique())
unique_no_oth = len(oth.Name.unique())
unique_no_fnv = len(fnv.Name.unique())
unique_no_milk = len(milk.Name.unique())

total_num_women = flat[flat.Ethnicity.str.contains('FE|FA|FO') == True].count()

unique_celebs_flat = flat[flat.Name.duplicated() == False][['Name','Ethnicity','Age','Food and beverage category']]

total_num_women = unique_celebs_flat[unique_celebs_flat.Ethnicity.str.contains('FE|FA|FO')].count()
total_num_men = unique_celebs_flat[unique_celebs_flat.Ethnicity.str.contains('ME|MA|MO')].count()

total_num_white = unique_celebs_flat[unique_celebs_flat.Ethnicity.str.contains('^W.*')].count()
total_num_black = unique_celebs_flat[unique_celebs_flat.Ethnicity.str.contains('^B.*')].count()
total_num_latino = unique_celebs_flat[unique_celebs_flat.Ethnicity.str.contains('^L.*')].count()
total_num_asian = unique_celebs_flat[unique_celebs_flat.Ethnicity.str.contains('^A.*')].count()

age_group_10_20 = unique_celebs_flat.ix[ (unique_celebs_flat['Age'] >= 10) & (unique_celebs_flat['Age'] < 20) ]
age_group_20_30 = unique_celebs_flat.ix[ (unique_celebs_flat['Age'] >= 20) & (unique_celebs_flat['Age'] < 30) ]
age_group_30_40 = unique_celebs_flat.ix[ (unique_celebs_flat['Age'] >= 30) & (unique_celebs_flat['Age'] < 40) ]
age_group_40_50 = unique_celebs_flat.ix[ (unique_celebs_flat['Age'] >= 40) & (unique_celebs_flat['Age'] < 50) ]
age_group_50_60 = unique_celebs_flat.ix[ (unique_celebs_flat['Age'] >= 50) & (unique_celebs_flat['Age'] < 60) ]
age_group_60_70 = unique_celebs_flat.ix[ (unique_celebs_flat['Age'] >= 60) & (unique_celebs_flat['Age'] < 70) ]
age_group_70_80 = unique_celebs_flat.ix[ (unique_celebs_flat['Age'] >= 70) & (unique_celebs_flat['Age'] < 80) ]
age_group_80_90 = unique_celebs_flat.ix[ (unique_celebs_flat['Age'] >= 80) & (unique_celebs_flat['Age'] < 90) ]

age_group_10_20_count = age_group_10_20.count()
age_group_20_30_count = age_group_20_30.count()
age_group_30_40_count = age_group_30_40.count()
age_group_40_50_count = age_group_40_50.count()
age_group_50_60_count = age_group_50_60.count()
age_group_60_70_count = age_group_60_70.count()
age_group_70_80_count = age_group_70_80.count()

confection = unique_celebs_flat[unique_celebs_flat['Food and beverage category'].isin(['Confection or Savory Snack or Cereal'])]
QSR = unique_celebs_flat[unique_celebs_flat['Food and beverage category'].isin(['QSR'])]
unique_celebs_flat.loc[unique_celebs_flat['Food and beverage category'].isin(['Low-cal & no-cal beverages','Low cal and no cal beverages','Low-cal & no-cal beverage','Dairy','Dairy ','water','Fruits and Vegetables','Fruit and Vegetables','Milk']),'class'] = 'p'
unique_celebs_flat.loc[unique_celebs_flat['Food and beverage category'].isin(['SSB','QSR','Confection or Savory snack or Cereal','Confection or Savory Snack or Cereal','Cereal']),'class'] = 'n'



















